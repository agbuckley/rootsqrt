# rootsqrt

![](rootsqrt-banner.png)

Bringing ROOT's typographic excellence to the rest of the LaTeX world

From the real thing:

<img src="root-root.png" width="40%" style="margin-left: 2em" />

to ROOT-quality typographic enhancements in native LaTeX:

<img src="demo.png" width="40%" style="margin-left: 4em; border: 1px solid; box-shadow: 5px 10px 18px #888888;" />

## Getting started

## Installation
Copy `rootsqrt.sty` into your LaTeX search path. Profit.

## Contributing
Guess what? This project isn't a priority. But if you make little tweaks that amuse me, make an MR!

## Authors
Andy Buckley

## License
LaTeX license
